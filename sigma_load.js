/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Stwórz metodę która będzie wywołana po kliknięciu buttona dodaj (dla krawędzi oraz dla wierzchołków)
// stwórz metodę "odrysuj" która spowoduje ponowne narysowanie grafu na płaszczyźnie


document.getElementById("btn_x").addEventListener('click', function(){
    var data = {};
    var node = {};
    node["id"] = "n0";
    node["label"] = "node1";
    node["x"] = 1;
    node["y"] = 2;
    node["size"] = 2;
    
    var node2 = {};
    node2["id"] = "n1";
    node2["label"] = "node2";
    node2["x"] = 2;
    node2["y"] = 3;
    node2["size"] = 3;
    
    var nodes = [];
    nodes.push(node);
    nodes.push(node2);
    
    var edge = {};
    edge["id"] = "e0";
    edge["source"] = "n0";
    edge["target"] = "n1";
    var edges = [];
    edges.push(edge);
    
    data["nodes"] = nodes;
    data["edges"] = edges;
    
    console.log(data);
//    sigma.parsers.json(data, {
//        container: 'sigma-container',
//        settings: {
//            defaultNodeColor: '#ec5148'
//        }
//    }); 
    var s = new sigma({ 
            graph: data,
            container: 'sigma-container',
            settings: {
                defaultNodeColor: '#ec5148'
            }
    });
});