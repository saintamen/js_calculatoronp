var rownanie = "4 - 5 * ( 2 + 10 )"


var Stack = function(e){
    var self = this;
    var last;
    var size = 0;
    
    self.get = function(index){
      // stwórz licznik
      var counter = 0;
      // stwóz zmienną tymczasowa
      var tmp = last;
      // dopóki licznik nie jest równy indeks szukany
      while(counter != index){
        if(tmp == null){
          console.log("Podany indeks nie istnieje");
          return null;
        }
        tmp = tmp.next;
        
        counter ++;
      }
      if(tmp == null){
        console.log("Podany indeks nie istnieje");
        return null;
      }
      return tmp.value;
      
    }
    
    self.add = function ( value ){
      size ++;
      var newNode = new Node(value);
      // tworzymy wezel
      if(last == null ){
        last = newNode;
      }else if(last !=null ){
        last.prev = newNode;
        newNode.next = last;
        last = newNode;
      }
    }
    
    self.remove = function (){
      if(last == null){
        return;
      }
      last = last.next;
      
      if(last != null){ // zabezpieczenie po usunieciu ostatniego el.
        last.prev = null;
      }
      if(size > 0){
        size --;
      }
    }
    
    self.pop = function (){
      var tmp = last;
      self.remove();
      if(tmp != null){
        return tmp.value;
      }else{
        return null;
      }
    }
    
    self.size = function(){
      return size;
    }
    
    var Node = function(value){
      this.value = value;
      var next= null;
      var prev = null;
    }
}

var ONP = function (e){
  var self = this;
  
  self.isOperand = function(what){
    if(what == '+' || what == '-' || what == '*' || what == '/' || what =='(' || what == ')'){
      return false;
    }else{
      return true;
    }
  }
  
  self.getPriority = function(from){
    if(from == '*'){
      return 2;
    }else if(from == '-'){
      return 1;
    }else if(from == '/'){
      return 2;
    }else if(from == '+'){
      return 1;
    }
  }
  
  self.doCalculation = function(token, op1, op2){
    if(token == '*'){
      var o1 = parseFloat(op1);
      var o2 = parseFloat(op2);
      return o1 * o2;
    } else if(token == '-'){
      var o1 = parseFloat(op1);
      var o2 = parseFloat(op2);
      return o2 - o1;
    }else if(token == '+'){
      var o1 = parseFloat(op1);
      var o2 = parseFloat(op2);
      return o1 + o2;
    }else if(token == '/'){
      var o1 = parseFloat(op1);
      var o2 = parseFloat(op2);
      return o2 / o1;
    }
  }
  
  self.countONP = function (equation){
    var s = new Stack();
    var res = equation.split(" ");
    for(var i=0; i<res.length; i++){
      var token = res[i];
      if(self.isOperand(token)){
        s.add(token);
      }else{
        var op1 = s.pop();
        var op2 = s.pop();
        var result = self.doCalculation(token, op1, op2);
        // console.log(result);
        s.add(result);
      }
    }
    var finalResult = s.pop();
    return finalResult;
  }
  
  self.convertToONP = function(equation){
    var onp = "";
    var s = new Stack();
    var res = equation.split(" ");
    for(var i=0; i<res.length; i++){
      var character = res[i];
      // console.log(character);
      if(self.isOperand(character)){
        onp+=character+" ";
      }else if(character == '('){
        s.add(character);
      }else if(character == ')'){
        while(s.size()>0){
          var tkn = s.pop();
          if(tkn == '('){
            break;
          }else{
            onp += tkn+" ";
          }
        }
      }else{
        var priority = self.getPriority(character);
        while(s.size() > 0){
          var poped = s.pop();
          if(poped == '(' || self.getPriority(poped) < priority){
            s.add(poped);
            break;
          }
          onp +=poped+" ";
        }
        s.add(character);
      }
    }
    
    while(s.size()>0){
      var item = s.pop();
      onp+=item+" ";
    }
    console.log(onp.slice(0, -1));
    // console.log();
    return onp.slice(0, -1);
  }
  
  self.oblicz = function(equation){
    return self.countONP(self.convertToONP(equation));
  }
}

document.getElementById("countValue").addEventListener('click', function(){
    var val = document.getElementById("input_field").value;
    
    // liczenie
    var calculator = new ONP();
    var wynik = calculator.oblicz(val);
    
    document.getElementById("input_field").value = wynik;
});